import {
    GET_ARTICLES_REQUESTED,
    GET_DETAIL_ARTICLES_REQUESTED,
    SET_SELECTED_ARTICLE,
    CLEAR_SELECTED_ARTICLE,
    CLEAR_ERROR,
} from './actionTypes';

export function getArticles(page) {
    return {
      type: GET_ARTICLES_REQUESTED,
      payload: page
    }
  }

export function getDetailArticle(contry) {
    return {
        type: GET_DETAIL_ARTICLES_REQUESTED,
        payload: contry
    }
}

export function openModal(article) {
    return {
        type: SET_SELECTED_ARTICLE,
        payload: article
    }
}

export function clearSelectedArticle(article) {
    return {
        type: CLEAR_SELECTED_ARTICLE,
    }
}


export function clearError() {
    return {
        type: CLEAR_ERROR,
    }
}