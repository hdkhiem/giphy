import React from 'react';
import PropTypes from 'prop-types';
import { formatDate } from 'services';
import './ListItem.scss';

const ListItem = ({ id, images, user, import_datetime, onClick }) => (
  <div className="item col-6  col-md-4  col-lg-3" onClick={() => onClick(id)} key={id} >
      <div className="bg-white wrapperImg">
        <img className='thumbnail' src={images.fixed_height_small_still.url} />
        <p>
          <span className="font-italic text-muted">{formatDate(import_datetime)}</span>
        </p>
      </div>
      <p className="text-left">
        { user && user.avatar_url && 
          <img className='avarta' src={user.avatar_url} />
        }
        <span className="font-weight-bold font-italic text-primary username">{user && user.username}</span>
      </p>
  </div>
)

ListItem.propTypes = {
  snippet: PropTypes.string,
  multimedia: PropTypes.string,
  pub_date: PropTypes.string,
  source: PropTypes.string,
  onClick: PropTypes.func.isRequired,
}

ListItem.defaultProps = {
  snippet: '',
  multimedia: '',
  pub_date: '',
  source: ''
}

export default ListItem;