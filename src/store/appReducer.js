import {
  GET_ARTICLES_SUCCEEDED,
  GET_ARTICLES_FAILED,
  SET_SELECTED_ARTICLE,
  CLEAR_SELECTED_ARTICLE,
} from 'action/actionTypes'

export const ARTICLES = 'articles';
export const SELECTED_ARTICLE = 'selectedArticle';
export const ERROR = 'error';

const appReducer = (state = {}, action) => {
  switch (action.type) {
  case GET_ARTICLES_SUCCEEDED:
    return {
        ...state,
        articles: action.payload
    };
  case GET_ARTICLES_FAILED:
    // TODO: handling for case get aritcle has been failed
    return {
      ...state,
      error: action.payload
    };
  case SET_SELECTED_ARTICLE:
    return {
        ...state,
        selectedArticle: action.payload
    };
  case CLEAR_SELECTED_ARTICLE: {
    return {
        ...state,
        selectedArticle: null
    };
  }
  default:
    return state;
  }
};

export default appReducer;
