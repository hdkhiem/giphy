import axios from 'axios';
import {
    ARTICLES_URI
} from 'const/uri';
import {
    KEY,
    LIMIT,
} from 'const'

export function getArticlesApi() {
    return axios.get(ARTICLES_URI, { params: { 'api_key': KEY, limit: LIMIT } });
}
