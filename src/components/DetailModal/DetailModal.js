import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { formatDate } from 'services';
import { clearSelectedArticle } from 'action/actionCreators';
import { SELECTED_ARTICLE } from 'store/appReducer';
import './DetailModal.scss';

class DetailModal extends PureComponent {
  constructor (props) {
    super(props);
    this.state = {
      showModal: false
    };
    
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }
  
  handleCloseModal () {
    this.setState({ showModal: false });
    this.props.clearSelectedArticle();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedArticle) {
      this.setState({ showModal: true });
    } else {
      this.setState({ showModal: false });
    }
  }

  render () {
    const selectedArticle = this.props.selectedArticle || {};

    return (
      <Modal
        isOpen={this.state.showModal}
        onRequestClose={this.handleCloseModal}
        shouldCloseOnOverlayClick={true}
      >
        {/* <div className='row'>
          <div className='col-12'>
            <h4>{selectedArticle.snippet}</h4>
            <p>
              <span>Date: {formatDate(selectedArticle.pub_date)}</span>
            </p>
            <p>
              <span>Source: {selectedArticle.source}</span>
            </p>
          </div>
        </div>
        <div className='row'>
          <div className='col-12 m-auto text-center'>
            <img className='media' src={selectedArticle.media} />
          </div>
        </div> */}
        {
          selectedArticle.images  &&
          <img className="media" src={selectedArticle.images.original.url} />
        }
        
        <button type="button" className="close" aria-label="Close" onClick={this.handleCloseModal}>
          <span aria-hidden="true">&times;</span>
        </button>
      </Modal>
    )
  }
}

DetailModal.propTypes = {
  selectedArticle: PropTypes.shape({
    snippet: PropTypes.string,
    media: PropTypes.string,
    pub_date: PropTypes.string,
    source: PropTypes.string,
  }),
  clearSelectedArticle: PropTypes.func.isRequired,
}

DetailModal.defaultProps = {
  selectedArticle: {},
}

const mapStateToProps = state => {
  return {
    selectedArticle: state.appReducer[SELECTED_ARTICLE],
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      clearSelectedArticle,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(DetailModal);