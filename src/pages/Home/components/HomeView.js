import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getArticles, openModal } from 'action/actionCreators'
import { ARTICLES } from 'store/appReducer';
import PropTypes from 'prop-types';
import { filter } from 'lodash';
import ListItem from 'components/ListItem/ListItem';
import DetailModal from 'components/DetailModal/DetailModal';
import './HomeView.scss';
import ArticlesCollection from '../../../components/ArticlesCollection/ArticlesCollection';

const TOTAL_PAGE = 200;
const VISIBLE_PAGE = 5;

export class HomeView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedArticle: {},
      isDetailOpen: false,
      currentPage: 1,
    };

    this.onItemClick = this.onItemClick.bind(this);
    this.handlePageChanged = this.handlePageChanged.bind(this);
  }

  componentDidMount() {
    this.props.getArticles();
  }

  onItemClick(id) {
    // Open the Detail modal view
    let selectedArticle = filter(this.props.articles, { id });

    if (selectedArticle && selectedArticle.length > 0 ) {
      this.props.openModal(selectedArticle[0])
    }
  }

  handlePageChanged(page) {
    this.setState({ currentPage : page});
    this.props.getArticles(page - 1);
  }

  render() {
    const { articles } = this.props;
    const { isDetailOpen, selectedArticle } = this.state;

    if (!articles) return (<h1>LOADING ...!</h1>);

    return (
      <div>
        <div className='m-3'>
          <img className='logo' src="https://netizency.files.wordpress.com/2018/01/api_giphy_logo_sparkle_clear.gif" alt="Giphy" border="0"></img>
        </div>
        <ArticlesCollection
          articles={articles}
          onItemClick={this.onItemClick}
        />
        <DetailModal/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    articles: state.appReducer[ARTICLES],
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getArticles,
      openModal,
    },
    dispatch,
  );

HomeView.propTypes = {
  articles: PropTypes.array,
  openModal: PropTypes.func.isRequired,
  getArticles: PropTypes.func.isRequired,
}

HomeView.defaultProps = {
  articles: null,
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
