import React from 'react';
import PropTypes from 'prop-types';
import ListItem from 'components/ListItem/ListItem';
import { getMedia, MEDIA_THUMBNAIL } from 'services';

const ArticlesCollection = ({ articles, onItemClick }) => (
  <div className='mb-5 row'>
    { articles.map(a =>
      <ListItem
        key={a.id}
        {...a}
        onClick={onItemClick}
          />
      ) }
  </div>
)

ArticlesCollection.propTypes = {
  articles: PropTypes.array,
  onItemClick: PropTypes.func,
}

ArticlesCollection.defaultProps = {
  articles: [],
  onItemClick: () => {},
}

export default ArticlesCollection;