import { call, put, takeEvery, takeLatest, fork } from 'redux-saga/effects'
import {
    GET_ARTICLES_REQUESTED,
    GET_ARTICLES_SUCCEEDED,
    GET_ARTICLES_FAILED,
    CLEAR_DETAIL_ARTICLES
 } from 'action/actionTypes'
import { getDetailArticleApi, getArticlesApi } from '../api'

function* getArticles(action) {
    try {
       const response = yield call(getArticlesApi, action.payload);
       yield put({type: GET_ARTICLES_SUCCEEDED, payload: response.data.data});
    } catch (e) {
       yield put({type: GET_ARTICLES_FAILED, payload: e.message});
    }
 }
 
 function* watchGetArticles() {
   yield takeLatest(GET_ARTICLES_REQUESTED, getArticles);
 }

export function* mySaga() {
    yield fork(watchGetArticles);
}

export default mySaga;
